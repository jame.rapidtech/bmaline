FROM node:14.0.0 AS build-env

WORKDIR /tmp
COPY . .
RUN npm install \
    && npm run build

FROM nginx:latest
EXPOSE 80
COPY --from=build-env /tmp/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d