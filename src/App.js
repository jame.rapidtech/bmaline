import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Register from './Components/login/Register';
import React from 'react';
import Profile from './Components/profile/Profile';
import { Routes, Route, HashRouter } from 'react-router-dom';
import Container from 'react-bootstrap/Container';

const App = () => {
  return (
    <div className='App'>
      {/* <Container>
      <Register />
      </Container> */}
      <Container className>
        <HashRouter>
          <Routes>
            <Route path={'/'} element={<Register />} />
            <Route path={'/a'} element={<Register />} />
            <Route path={'/b'} element={<Register />} />
            <Route path={'/c'} element={<Register />} />
            <Route path={'/d'} element={<Register />} />
            <Route path={'/e'} element={<Register />} />
            <Route path={'/f'} element={<Register />} />
            <Route path={'/profile'} element={<Profile />} />
          </Routes>
        </HashRouter>
      </Container>
    </div>
  );
};

export default App;
