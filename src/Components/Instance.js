import React from "react";
import axios from "axios";
// import https from "https";
// const https = require('https');

const instance = axios.create({
  baseURL: 'https://manager.rapidtech.app/bma-line-manager/api', 
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Max-Age': '86400',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Content-Type': 'application/json'
}
});

// const instance = axios.create({
//   baseURL: 'http://localhost:3000/bma-line-manager/api', 
//   headers: {
//     'Access-Control-Allow-Origin': '*',
//     'Access-Control-Max-Age': '86400',
//     'Access-Control-Allow-Credentials': 'true',
//     'Access-Control-Allow-Headers': 'Content-Type',
//     'Content-Type': 'application/json'
// }
// });


export { instance }