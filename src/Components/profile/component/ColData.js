import { Col } from 'antd';
import React from 'react';
import css from './ColData.module.css'

const ColData = (props) => {
  const {topic,data} = props;
  return (
    <Col className={css.colData} span={16} offset={1}>
      <label className={css.textHeadData}>{topic}</label>
      <p className={css.textData}>{data}</p>
    </Col>
  );
};

export default ColData;
