import React, { useState, useEffect } from 'react';
// import liff from '@line/liff/dist/lib';
import { Col, Button } from 'antd';
import css from './Profile.module.css';

const Profile = (props) => {
  const { photo, name, status, showProfile } = props;
  const [linePhoto, setLinePhoto] = useState('');
  const [lineDisplayName, setLineDisplayName] = useState('');
  // const [idCard, setIDcard] = useState('');
  // const [agency, setAgency] = useState('');
  // const [affiliation, setAffiliation] = useState('');
  // const [position, setPosition] = useState('นั');
  // const [level, setLevel] = useState('');
  // const [type, setType] = useState('');
  const [statusDisplay, setStatusDisplay] = useState(true);

  useEffect(() => {
    // initLine()
    setLinePhoto(photo);
    setLineDisplayName(name);
    setStatusDisplay(status);
    // setIDcard('9999999999999')
    // setAgency('สำนักงานคณะกรรมการข้าราชการกรุงเทพมหานคร')
    // setAffiliation('กรุงเทพมหานคร')
    // setPosition('กจัดการงานทั่วไป')
    // setLevel('ชำนาญงาน')
    // setType('ข้าราชการกรุงเทพมหานครสามัญ')

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Col className={css.center}>
        <p className={statusDisplay === true ? css.status : css.status2}>{statusDisplay === true ? 'ลงทะเบียนสำเร็จ' : 'ลงทะเบียนไม่สำเร็จ'}</p>

        {statusDisplay === true ? (
          <p className={css.text}>
            ขอบคุณที่มาเป็นเพื่อนกับผู้ว่าฯ
            <br />
            สำนักงานประชาสัมพันธ์ กรุงเทพมหานคร
          </p>
        ) : (
          <p className={css.text}>
            ไม่พบข้อมูล โปรดตรวจสอบวันเดือนปีเกิด
            <br />
            และเลขบัตรประชาชนของคุณอีกครั้ง
          </p>
        )}
      </Col>
      {statusDisplay === true ? (
        <>
          <Col className={css.imagebox}>
            <img src={linePhoto} className={css.image} alt='' />
            <p className={css.labelName}>{lineDisplayName}</p>
          </Col>
          {/* <ColData topic='เลขบัตรประชาชน' data={idCard} />
      <ColData topic='หน่วยงาน' data={agency} />
      <ColData topic='สังกัด' data={affiliation} />
      <ColData topic='ตำแหน่ง' data={position} />
      <ColData topic='ระดับ' data={level} />
      <ColData topic='ประเภท' data={type} />{' '} */}
        </>
      ) : (
        <>
          {' '}
          <Button className={css.buttonSubmit} type='primary' htmlType='submit' onClick={showProfile}>
            ลงทะเบียนอีกครั้ง
          </Button>
        </>
      )}
    </div>
  );
};
export default Profile;
