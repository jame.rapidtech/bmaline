import React, { useEffect, useState, useRef } from 'react';
import { instance } from '../Instance';

import { Button, Checkbox, Col, Form, Input, Row } from 'antd';
// import { useNavigate } from 'react-router-dom';
import css from './Register.module.css';
import moment from 'moment';
import axios from 'axios';
import liff from '@line/liff/dist/lib';
import Profiles from '../profile/Profile';
import PrivacyModal from '../privacy/PrivacyModal';

const Register = () => {
  // const [idToken, setIdToken] = useState('');
  const [status, setStatus] = useState('');
  const [statusProfile, setStatusProfile] = useState('');
  const [idCard, setIdCard] = useState('');
  const [linePhoto, setLinePhoto] = useState();
  const [lineDisplayName, setLineDisplayName] = useState('');
  const [lineUserId, setLineUserId] = useState('');
  const [lineEmail, setLineEmail] = useState('');
  const [checkbox, setCheckbox] = useState(false);
  const [ip, setIP] = useState('');
  const [isModalOpen, setIsModalOpen] = useState('');
  const [isButton, setIsButtton] = useState(true);
  const [hide, sethide] = useState(false);
  const [page, setPage] = useState('');
  const [regisLoading, setRegisLoading] = useState(false);
  const formRef = useRef();
  const [form] = Form.useForm();
  const inputDRef = useRef(null);
  const inputMRef = useRef(null);
  const inputYRef = useRef(null);
  const inputIDRef = useRef(null);
  const href = window.location.href;

  useEffect(() => {
    // searchParams.get("__firebase_request_key")

    initLine();
    // setStatus(false);
    // getIdLine();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getIP = async () => {
    const res = await axios.get('https://geolocation-db.com/json/');
    setIP(res.data.IPv4);
  };

  const initLine = () => {
    liff.init(
      { liffId: '1660739917-3o599v2q' },
      () => {
        if (liff.isLoggedIn()) {
          runApp();
        } else {
          liff.login();
        }
      },
      (err) => console.log.error(err)
    );
  };

  const runApp = async () => {
    // const idTokens = liff.getIDToken();
    // setIdToken(idTokens);

    await liff
      .getProfile()
      .then((profile) => {
        console.log(profile);
        setLinePhoto(profile.pictureUrl);
        setLineDisplayName(profile.displayName);
        getIdLine(profile.userId);
        setLineUserId(profile.userId);
        setLineEmail(liff.getDecodedIDToken().email);
      })
      .catch((err) => console.error(err));
    console.log('lineUserId = ', lineUserId);

    await getIP();
  };

  const getIdLine = async (lineid) => {
    console.log('start gitid');
    try {
      await instance
        .post('/lineid', { line_user_id: lineid, menu_code: href.match(/([^/]*)\/*$/)[1] })
        .then((response) => {
          console.log('send = ', lineUserId);
          console.log('response = ', response.data);
          console.log('Line id = ', response.data.line_user_id);
          if (response.data.line_user_id === true) {
            window.location.replace(response.data.url);
            setPage(response.data.url);
          } else {
            console.log();
            setStatus(false);
            setPage(response.data.url);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    } catch (error) {}
    console.log('end gitid');
  };

  const validateidCard = (_, value) => {
    const regex = new RegExp(/^[0-9]{5}$/g);
    if (idCard !== '') {
      if (regex.test(value)) {
        return Promise.resolve();
      } else {
        return Promise.reject(new Error(`โปรดระบุตัวเลขให้ถูกต้อง`));
      }
    } else {
      return Promise.reject(new Error(`โปรดระบุเลขบัตรประชาชน 5 หลักสุดท้าย`));
    }
  };

  const onChangeCheckbox = (e) => {
    setCheckbox(!checkbox);
    setIsButtton(!isButton)
    sethide(false);
  };

  const changeFormattDate = (value) => {
    if (value !== '' && value <= 9) {
      value = `0${value}`.replace(/0*/, '0');
      return value;
    } else {
      return value;
    }
  };

  const handleKeyDown = (e) => {
    let keyCode = e.key.charCodeAt(0);
    if ((keyCode >= 48 && keyCode <= 57) || keyCode === 65 || keyCode === 66) {
      return true;
    } else {
      e.preventDefault();
    }
  };

  const validatorDay = (rule, value) => {
    if (parseFloat(value) > 31 || parseFloat(value) <= 0) {
      return Promise.reject(new Error('วันที่ไม่ถูกต้อง'));
    }

    return Promise.resolve();
  };

  const validatorMonth = (rule, value) => {
    if (parseFloat(value) > 12 || parseFloat(value) <= 0) {
      return Promise.reject(new Error('เดือนไม่ถูกต้อง'));
    }

    return Promise.resolve();
  };

  const validatorYear = (rule, value) => {
    if (parseFloat(value) < 2400) {
      return Promise.reject(new Error('กรุณาระบุปี พ.ศ'));
    }

    if (parseFloat(value) > moment().year() + 543) {
      return Promise.reject(new Error('โปรดระบุอายุถูกต้อง'));
    }

    return Promise.resolve();
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleClose = () => {
    setIsModalOpen(false);
  };

  const onFinish = async (values) => {
    values.year = values.year - 543;

    const dataset = {
      birth_date: values.year.toString() + '-' + changeFormattDate(values.month) + '-' + changeFormattDate(values.day),
      id_card_number: values.idCard,
      line_photo: linePhoto,
      line_user_id: lineUserId,
      line_display_name: lineDisplayName,
      line_email: lineEmail,
      ip: ip,
      accepted_pdpa: checkbox,
    };
    console.log('dataset', dataset);
    console.log('dataset Birth_date', dataset.birth_date);
    setRegisLoading(true)
    setIsButtton(true)
    setTimeout(async () => {
      try {
        console.log('start');
        await instance
          .post('/registrationfull', dataset)
          .then((response) => {
            console.log(response.data.register_success);
            setStatusProfile(response.data.register_success);
            if (response.data.register_success === true) {
              setTimeout(() => {
                window.location.replace(page);
              }, 3000);
            }
          })
          .catch((error) => {
            console.error(error);
          });
        console.log('end');
      } catch (error) {
        console.log(error.stack);
      }
      setStatus(true);
    }, 2000);

    // window.location.replace('https://www.google.com/');
  };

  const handleDayKeyUp = (event) => {
    if (event.target.value.length === 2) {
      inputMRef.current.focus();
    }
  };

  const handleDayblur = (event) => {
    if (event.target.value.length === 1) {
      event.target.value = '0' + event.target.value;
    }
    form.setFieldsValue({ day: event.target.value });
  };

  const handleMonthKeyUp = (event) => {
    if (event.target.value.length === 2) {
      inputYRef.current.focus();
    }
  };

  const handleMonthblur = (event) => {
    if (event.target.value.length === 1) {
      event.target.value = '0' + event.target.value;
    }
    form.setFieldsValue({ month: event.target.value });
  };

  const handleYearKeyUp = (event) => {
    if (event.target.value.length === 4) {
      inputIDRef.current.focus();
    }
  };

  const checkStatusDisplay = () => {
    setStatus(false);
    setCheckbox(false);
    setIsButtton(true);
    setRegisLoading(false);
    form.resetFields();
  };

  return (
    <>
      {status === false && (
        <div>
          <div className={css.rowlineName}>
            <img src={linePhoto} className={css.image} alt='' />

            <p className={css.labelName} style={{ fontSize: '18px', fontWeight: 'bold', color: '#050505' }}>
              {lineDisplayName}
            </p>
          </div>

          <Form name='form-register' layout='vertical' form={form} onFinish={onFinish} ref={formRef} initialValues={{ remember: checkbox }}>
            <p className={css.itemlabelName}>
              <label className={css.itemStarLabel}>*</label> วันเดือนปีเกิด <label className={css.itemDateLabel}>(วว/ดด/ปปปป) </label>
            </p>
            <Row justify='space-between' gutter={[12, 24]}>
              <Col xs={8} sm={12} md={8} lg={8} xl={8}>
                <Form.Item
                  name='day'
                  rules={[
                    {
                      required: true,
                      message: 'โปรดระบุวัน',
                    },
                    {
                      validator: validatorDay,
                    },
                  ]}
                >
                  <Input
                    ref={inputDRef}
                    autoFocus={true}
                    style={{ width: '100%', borderRadius: '8px', fontSize: '16px' }}
                    className='itemInput'
                    name='inday'
                    placeholder='วันที่'
                    maxLength={2}
                    onBlur={handleDayblur}
                    onKeyDown={handleKeyDown}
                    onKeyUp={handleDayKeyUp}
                  />
                </Form.Item>
              </Col>

              <Col xs={8} sm={12} md={8} lg={8} xl={8}>
                <Form.Item
                  name='month'
                  rules={[
                    {
                      required: true,
                      message: 'โปรดระบุเดือน',
                    },
                    {
                      validator: validatorMonth,
                    },
                  ]}
                >
                  <Input
                    ref={inputMRef}
                    style={{ width: '100%' }}
                    className='itemInput'
                    placeholder='เดือน'
                    maxLength={2}
                    onKeyDown={handleKeyDown}
                    onKeyUp={handleMonthKeyUp}
                    onBlur={handleMonthblur}
                  />
                </Form.Item>
              </Col>

              <Col xs={8} sm={12} md={8} lg={8} xl={8}>
                <Form.Item
                  name='year'
                  rules={[
                    {
                      required: true,
                      message: 'โปรดระบุปี',
                    },
                    {
                      validator: validatorYear,
                    },
                  ]}
                >
                  <Input
                    ref={inputYRef}
                    style={{ width: '100%' }}
                    className='itemInput'
                    placeholder='ปี พ.ศ.'
                    maxLength={4}
                    onKeyDown={handleKeyDown}
                    onKeyUp={handleYearKeyUp}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <p className={css.itemlabelName}>
                  <label className={css.itemStarLabel}>* </label> เลขบัตรประชาชน 5 หลักสุดท้าย
                </p>
                <Form.Item
                  name='idCard'
                  rules={[
                    {
                      validator: validateidCard,
                    },
                  ]}
                >
                  <Input
                    ref={inputIDRef}
                    className='itemInput'
                    placeholder='โปรดระบุเลขบัตรประชาชน 5 หลัก'
                    disabled={false}
                    onChange={(e) => setIdCard(e.target.value)}
                    maxLength={5}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row justify='center' align='middle'>
              <Col span={24}>
                <Form.Item name='remember' valuePropName='checked'>
                  {hide === true ? <Checkbox checked={checkbox}></Checkbox> : null}

                  <Checkbox onChange={onChangeCheckbox} checked={checkbox}>
                    <Row align='middle'>
                      <Col span={12}>
                        <label className={css.checkbox} style={{ fontSize: '16px' }}>
                          ยอมรับเงื่อนไขตาม
                        </label>
                      </Col>
                      <Col span={12}>
                        <Button
                          type='text'
                          onClick={showModal}
                          style={{ color: '#025937', fontSize: '16px', marginLeft: '-30px', marginTop: '-3.5px' }}
                        >
                          นโยบายความเป็นส่วนตัว
                        </Button>
                      </Col>
                    </Row>
                  </Checkbox>
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Form.Item>
                  <Button className={css.buttonSubmit} type='primary' htmlType='submit' disabled={isButton} loading={regisLoading}>
                    ลงทะเบียน
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      )}

      {status === true && (
        <>
          <Profiles photo={linePhoto} name={lineDisplayName} showProfile={checkStatusDisplay} status={statusProfile} />
        </>
      )}

      <PrivacyModal statusModal={isModalOpen} onClose={handleClose} onChangeCheckbox={onChangeCheckbox} />
    </>
  );
};

export default Register;
